House rules:

0: Assumes use of git (with VI for commits), gradle as well as gitlab

1: All commits must be made against an issue number
   place the issue number reference to the left of the commit message first line
   then follow up with some english headline (brief)
   
   leave the second line blank, add detailed points in the third and lower lines

2: All commits must be of a build which compiles 
   (at least on the committers local machine)
   
